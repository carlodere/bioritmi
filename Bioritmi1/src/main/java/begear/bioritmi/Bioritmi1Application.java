package begear.bioritmi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bioritmi1Application {

	public static void main(String[] args) {
		SpringApplication.run(Bioritmi1Application.class, args);
	}

}
